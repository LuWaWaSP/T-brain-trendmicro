from collections import defaultdict, Counter

import pickle
import glob
import os

import pandas as pd
import numpy as np

if __name__ == "__main__":
    file_id_all_logs = defaultdict(dict)
    logs_files = sorted(glob.glob('query_log/*.csv'))
    # union all prod_id pickle files (92files)
    # [warning]  running tihs cell require 40GB RAM extreme large!!
    for logs_file in logs_files:
        #prod
        with open(logs_file + "_raw_logs.pkl","rb") as f:
            file_id_logs = pickle.load(f)
        
        for file_id, logs_dir in file_id_logs.items():
            if file_id not in file_id_all_logs:  # handle file
                file_id_all_logs[file_id] = logs_dir
                continue
            logs_dir_all = file_id_all_logs[file_id]
            for id_pair, values in logs_dir.items():
                    if id_pair not in logs_dir_all:  # handle id_pair
                        logs_dir_all[id_pair] = values
                        continue
                    logs_dir_all[id_pair]['counts'] += values['counts']
                    logs_dir_all[id_pair]['queyts_list'].extend(values['queyts_list'])
    
    # [warning] tihs file require ~3GB to save. since the object is too large, pickle with protocol = 4
    if not os.path.exists('cache'):
        os.makedirs('cache')
    with open("cache/file_id_all_logs.pkl", "wb") as f:
        pickle.dump(file_id_all_logs, f, protocol=4)