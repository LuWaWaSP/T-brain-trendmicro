import pickle
from collections import defaultdict, Counter

import arrow
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.stats import kurtosis, skew

def create_statistics(values, prefix):
    """
    calculate var, mean, median, max, min ,p25, p75, skew, kutosis
    
    Parameters
    ----------
    values: List[float]
        list of values used to generate statistics
    prefix: str
        used to create key of return dict

    Return 
    ----------
    statistics_log : Dict[str, float]
        return statistic of the given list, and key with prefix
    
    """
    statistics_log = {}
    
    _var = np.var(values)
    _mean = np.mean(values)
    _med = np.median(values)
    _max = np.max(values)
    _min = np.min(values)
    _p25 = np.percentile(values, 25)
    _p75 = np.percentile(values, 75)
    _skew = skew(values)
    _kurtosis = kurtosis(values)
    
    statistics_log[prefix + '_var'] = _var
    statistics_log[prefix + '_med'] = _med
    statistics_log[prefix + '_mean'] = _mean
    statistics_log[prefix + '_max'] = _max
    statistics_log[prefix + '_min'] = _min
    statistics_log[prefix + '_p25'] = _p25
    statistics_log[prefix + '_p75'] = _p75
    statistics_log[prefix + '_diff'] = _max - _min
    statistics_log[prefix + '_skew'] = _skew
    statistics_log[prefix + '_kurtosis'] = _kurtosis    
    return statistics_log


def feature_extraction(logs):
    """
    for each file_id extract naive features: 
        prodId, CustomId, queryts, queryts_interveal features 
    
    Parameters
    ----------
    logs: Dict[(str,str), Dict[str,]]
        key:  pair with (cu_id, prod_id)
        values: Dict[str]
            'counts': int
                pair count in logs
            'queryts': list
                pair timestamp of logs

    Return
    ----------
    features: Dict[str, float]
        feautres of this file_id
    """
    features = {}
    prod_counts = defaultdict(int)
    custom_counts = defaultdict(int)
    query_ts_a = []
    # unique_paris.append(logs.keys())  # ToDo remove after runing <- sec3
    for id_pair, values in logs.items():
        cu_id, prod_id = id_pair
        prod_counts[prod_id] += values['counts']  # pd feautres
        custom_counts[cu_id] += values['counts']  # cu features
        query_ts_a.extend(values['queyts_list'])  # query features
        
    query_ts_a = sorted(query_ts_a)

    # pu features
    prod_values = list(prod_counts.values())
    prod_sum = sum(prod_values)
    prod_n = len(prod_values)
    
    # log counts
    features['count'] = prod_sum
    
    prod_stat_dict = create_statistics(prod_values, 'prod', )
    
    # ------ prod features ------
    features.update(prod_stat_dict)
    features['prod_n'] = prod_n
    for prodid, value in prod_counts.items():
        features['prod_' + str(prodid)] = value

    # ------ customId features ------
    cu_values = list(custom_counts.values())
    cu_stat_dict = create_statistics(cu_values, 'cu')
    cu_n = len(cu_values)
    features.update(cu_stat_dict)
    features['cu_n'] = cu_n
    
    # ------ queryts features ------
    ts_stat_dict = create_statistics(query_ts_a, 'ts')
    features.update(ts_stat_dict)

    # ------ queryts interval features ------
    query_ts_a_interval = [j-i for i,j in  zip(query_ts_a, query_ts_a[1:])]
    if not query_ts_a_interval:
        query_ts_a_interval = [0]            
    ts_interval_stat_dict = create_statistics(query_ts_a_interval, 'ts_interval')
    features.update(ts_interval_stat_dict)    

    # ------ queryts Date features ------
    query_ts_arrow = [arrow.get(i) for i in query_ts_a]    
    start_date = query_ts_arrow[0]
    ts_weekday_start = start_date.weekday()
    ts_day_start = int(start_date.format("D"))
    ts_hour_start = int(start_date.format("M"))
    ts_month_start = int(start_date.format("H"))
    features['ts_weekday_start'] = ts_weekday_start
    features['ts_day_start'] = ts_day_start
    features['ts_hour_start'] = ts_hour_start
    features['ts_month_start'] = ts_month_start
    
    # get end_date , end_date month, weekday, hour, day
    end_date = query_ts_arrow[-1]
    ts_weekday_end = end_date.weekday()
    ts_day_end = int(end_date.format("D"))
    ts_hour_end = int(end_date.format("M"))
    ts_month_end = int(end_date.format("H"))
    features['ts_weekday_end'] = ts_weekday_end
    features['ts_day_end'] = ts_day_end
    features['ts_hour_end'] = ts_hour_end
    features['ts_month_end'] = ts_month_end    

    # get Month/Weekday/Hour count
    weekdayC= Counter([i.weekday() for i in query_ts_arrow])
    weekdayC_term = sum(weekdayC.values())
    for key in range(7):
        features["ts_weekday_"+str(key)] = weekdayC.get(key, 0) / weekdayC_term

    MonthC= Counter([i.format("M") for i in query_ts_arrow])
    MonthC_term = sum(MonthC.values())
    for key in ["3", "4", "5"]:
        features["ts_month_"+str(key)] = MonthC.get(key, 0) / MonthC_term    

    DayC= Counter([i.format("D") for i in query_ts_arrow])
    DayC_term = sum(DayC.values())
    for key in [ str(i) for i in range(1, 32)]:
        features["ts_day_"+str(key)] = DayC.get(key, 0) / DayC_term        

    HourC= Counter([i.format("H") for i in query_ts_arrow])
    HourC_term = sum(HourC.values())
    for key in [str(i) for i in range(24)]:
        features["ts_hour_"+str(key)] = HourC.get(key, 0) / HourC_term     
    return features


if __name__ == "__main__":
    # [warning] reading this pickle file require ~40GB memory, fucking huge!!
    with open("cache/file_id_all_logs.pkl", "rb") as f:
        file_id_all_logs = pickle.load(f)

    file_id_features = defaultdict(dict)
    # unique_paris = []  # ToDo remove after runing <- sec3
    for file_id, logs in tqdm(file_id_all_logs.items()):
        file_id_features[file_id] = feature_extraction(logs)

    # cache the features
    file_id_features_X = pd.DataFrame.from_dict(file_id_features)
    file_id_features_X = file_id_features_X.transpose()
    file_id_features_X = file_id_features_X.fillna(-1)

    with open("cache/feature_naive_statistics","wb") as f:
        pickle.dump(file_id_features_X, f)