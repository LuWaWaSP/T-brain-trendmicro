import pickle

import pandas as pd 
import lightgbm as lgb
import scipy.stats as ss
from sklearn.cross_validation import StratifiedKFold

def get_test_data(features_X, testing_csv, useless_features=[]):
    """
    create test dataframe

    Parameters
    ----------
    features_X: pd.dataframe
        predtion of 10 folds
    testing_csv: pd.dataframe
        training_csv file
    useless_features: List[str]
        features, that not want ot use
    
    Return
    ----------
    test_X: pd.dataframe
        testing features
    """    
    test_id_raw = testing_csv
    test_idx = pd.Index(test_id_raw['FileID'])

    used_features = [ i for i in features_X.columns if i not in useless_features]  
    test_X = features_X.loc[test_idx, used_features]

    print("Train X shape:", test_X.shape)

    return test_X


def get_train_data(features_X, training_csv, useless_features=[]):
    """
    create train dataframe
    
    Parameters
    ----------
    features_X: pd.dataframe
        predtion of 10 folds
    training_csv: pd.dataframe
        training_csv file
    useless_features: List[str]
        features, that not want ot use
    
    Return
    ----------
    train_X: pd.dataframe
        training features
    train_Y: pd.series
        training labels
    """    
    train_id_raw = training_csv
    trian_idx = pd.Index(train_id_raw['FileID'])
    
    used_features = [i for i in  features_X.columns
                     if i not in useless_features]
    
    train_Y = train_id_raw['Label']
    train_X = features_X.loc[trian_idx, used_features]
    print("Train X shape:", train_X.shape)

    return train_X, train_Y


def get_fold_data(cv_idx, features_X, training_csv, useless_features=[]):
    """
    create cv data
    
    Parameters
    ----------
    cv_idx: List[np.array]
        index used for create training, validation
    features_X: pd.dataframe
        predtion of 10 folds
    training_csv: pd.dataframe
        training_csv file
    useless_features: List[str]
        features, that not want ot use
    
    Return
    ----------
    train_X: pd.dataframe
        training features
    train_Y: pd.series
        training labels
    valid_X: pd.dataframe
        training features
    valid_Y: pd.series
        training labels        
    """        
    train_id_raw = training_csv.iloc[cv_idx[0],:]
    trian_idx = pd.Index(train_id_raw['FileID'])
    valid_id_raw = training_csv.iloc[cv_idx[1],:]
    valid_idx = pd.Index(valid_id_raw['FileID'])
    
    used_features = [i for i in  features_X.columns
                     if i not in useless_features] #or i.startswith("corr_"))]
    train_X = features_X.loc[trian_idx, used_features]
    train_Y = train_id_raw['Label']
    valid_X = features_X.loc[valid_idx, used_features]
    valid_Y = valid_id_raw['Label']

    print("Train X shape:", train_X.shape)
    print("Train positive  Y:", sum(train_Y), len(train_Y))
    print("Valid X shape:", valid_X.shape)
    print("Valid positive  Y:", sum(valid_Y), len(valid_Y) )
    
    return [train_X, train_Y, valid_X, valid_Y]


def train_gbm(lgb_train, lgb_eval):
    """
    given training, validation lightgbm data set, return a trained model

    Parameters
    ----------
    lgb_train: lgb.Dataset
        training set
    lgb_eval: lgb.Dataset
        validation set
    
    Returns
    ----------
    gbm: lgb.Booster
        trained lgb model 
    """    
    params = {
        'task': 'train',
        'boosting_type': 'goss',
        'objective': 'binary',
        'metric': { 'auc'},
        'num_leaves': 63,
        'learning_rate': 4e-2,
        'feature_fraction': 0.85, 
        'bagging_freq': 5, 
        'verbose': 0,
    }

    gbm = lgb.train(params,
                    lgb_train,
                    num_boost_round=25000,
                    valid_sets=lgb_eval,
                    early_stopping_rounds=500)
    return gbm    


def creat_submission(y_hat_fold, testing_csv, file_name='submission_naive_statisitcs.csv'):
    """
    using rank sum to aggregate 10 fold testing results

    Parameters
    ----------
    y_hat_fold: List[np.array]
        predtion of 10 folds
    testing_csv: pd.dataframe
        testing file
    file_name: str
        outpupath os submission
    """
    y_rank_list = [ss.rankdata(y_hat) for y_hat in y_hat_fold]
    y_hat = sum(y_rank_list)
    y_hat = y_hat / max(y_hat)
    testing_csv['Label'] = y_hat
    testing_csv.to_csv("submission_naive_statisitcs.csv", header=None, index=None)


if __name__ == '__main__':
    testing_csv = pd.read_csv('testing-set.csv', header=None, names=['FileID','Label'])
    file_ids_testing =  set(testing_csv['FileID'])
    training_csv = pd.read_csv('training-set.csv', header=None, names=['FileID','Label'])
    file_ids_training =  set(training_csv['FileID'])
    with open('cache/feature_naive_statistics', 'rb') as f:
        features_X = pickle.load(f)
    
    # get testing X
    features_X_test = get_test_data(features_X, testing_csv)

    # get fold cv idx
    training_Y = training_csv.Label
    skf = StratifiedKFold(training_Y, n_folds=10, random_state=5566, shuffle=True)  # cv fold
    skf = [i for i in skf]
    
    cv_auc_fold = []  # 10 fold cv auc
    y_hat_fold = []  # testing results
    # 10 folds cv
    for fold in range(10):
        cv_idx = skf[fold]
        train_X, train_Y, valid_X, valid_Y = get_fold_data(cv_idx, features_X, training_csv)
        lgb_train = lgb.Dataset(train_X, train_Y)
        lgb_eval = lgb.Dataset(valid_X, valid_Y, reference=lgb_train)
        gbm = train_gbm(lgb_train, lgb_eval)
        cv_auc_fold.append(gbm.best_score['valid_0']['auc'])  # cv scores
        y_hat = gbm.predict(features_X_test)  # predict on testing set
        y_hat_fold.append(y_hat)

    creat_submission(y_hat_fold, testing_csv)  # create submission files
    
