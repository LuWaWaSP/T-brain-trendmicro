import time
import pickle
import glob
from multiprocessing import Pool
from collections import defaultdict

import pandas as pd

def extract_file_id_info(log_file):
    """
    given log_file of Tbrains: trend micro , extract log information for Cuid, QueryTs, ProductID
    and saving extracted info with pkl format : log_file + "_raw_logs.pkl"

    Parameters
    ----------
    log_file: str
        the log files (csv in query_log)
    """
    file_id_logs = defaultdict(dict)
    start_time = time.time()
    log_tmp = pd.read_csv(log_file, header=None, names=['FileID', 'CustomerID', 'QueryTS', 'ProductID'])

    # file cuid count , product count
    for _, row in log_tmp.iterrows():
        file_id = row['FileID']
        id_pair =  (row['CustomerID'], row['ProductID'])

        logs_dir = file_id_logs[file_id]
        if id_pair not in logs_dir:
            logs_dir[id_pair] = {'counts': 1, 'queyts_list': [row['QueryTS']]}
        else:
            logs_dir[id_pair]['counts'] += 1 
            logs_dir[id_pair]['queyts_list'].append(row['QueryTS'])
    
    with open(log_file + "_raw_logs.pkl", "wb") as f:
        pickle.dump(file_id_logs, f)
    print("[execution_time]--- %s require %s seconds ---" % (log_file ,time.time() - start_time))


if __name__ == "__main__":
    testing_csv = pd.read_csv('testing-set.csv', header=None, names=['FileID','Label'])
    file_ids_testing =  set(testing_csv['FileID'])
    print(testing_csv.shape)
    training_csv = pd.read_csv('training-set.csv', header=None, names=['FileID','Label'])
    file_ids_training =  set(training_csv['FileID'])
    logs_files = sorted(glob.glob('query_log/*.csv'))
    pool = Pool()
    # process cu, prod, query id to pikcles
    for res in pool.map(extract_file_id_info, logs_files):
        print("done")

