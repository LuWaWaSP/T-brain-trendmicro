# T-brains

put training/ testing  csv in root folder

log csvs in query_log folder (query_log/0301.csv ...)


it may require ~40 GB ram and ~3GB disk storage to Run following

## data preprocessing
```
python data_preprocessing_raw.py # loop over logs csvs, create raw_logs.pkl 
python data_preprocessing_aggregate.py # aggregate cuid logs
python feature_stage_naive.py # create feature_naive_statistics pd.dataframe:(81894, 145)
```
## model training - a lightgbm example
```
python train_lightgbm.py
```

if your computer can't afforid it, here is the link of [feature_naive_statistics](https://mega.nz/#!NrISTC6K!wd5XBuHG_NK6Y1VM21A87eI1bqW-8tne_XAEqlNy9Pw)

